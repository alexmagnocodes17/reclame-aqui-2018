This application expose some api's that provide access to clients complains that are saved in a NoSql database(Redis).

Premises:

	Must have a Redis running.

Execution:

Clone project from Bitbucket:

git clone git@bitbucket.org:alexmagnocodes17/reclame-aqui-2018.git

Run test:
mvn test

Run application:
mvn spring-boot:run

API's:

Save a complain:

http://localhost:8085/v1/redis/complains/save

FORMAT JSON to save new complain:
{ 
	"title" : "Operadora cancelou minha assinatura indevidamente",
	"description" : "Sistema sempre indisponível",
	"locale" :  "SP",
	"company" : "VIVO"
}

Recover list of entities from Redis:
http://localhost:8085/v1/redis/complains/entities

Recover list of entities as dto from Redis:
http://localhost:8085/v1/redis/complains

Recover list of dtos from Redis searching by locale and company:
http://localhost:8085/v1/redis/complains/searchBy?locale=SP&company=VIVO

DEVOPS:

Continuous Integration: 
	Jenkins:
	Create an Jenkins project;
	Configure repository(Bitbucket);
	Configure to build first others projects that are dependencies from main project;
	Configure automated tests.

Continuous Deployment:
	I never work with this tools. This work it was done by INFRA team.
	These tools as Ansible and Chef, are responsible to installation, maintenance and environment configuration of infrastructure to be done automatic way.
