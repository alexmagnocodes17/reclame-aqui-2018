package br.com.reclameaqui.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "complainant")
public class ComplainantEntity extends BaseEntity
{
    @Column(name = "name", nullable = false, length = 128)
    private String name;

    @Column(name = "email", nullable = false, length = 64)
    private String email;

    @OneToMany(cascade = CascadeType.ALL,
                    orphanRemoval = true)
    @JoinColumn(name = "complainant_id")
    private List<ComplainEntity> complains;

    public ComplainantEntity()
    {
        // TODO Auto-generated constructor stub
    }

    public ComplainantEntity(String name, String email)
    {
        setName(name);
        setEmail(email);
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return the email
     */
    public String getEmail()
    {
        return email;
    }

    /**
     * @param email
     *            the email to set
     */
    public void setEmail(String email)
    {
        this.email = email;
    }

    /**
     * @return the complains
     */
    public List<ComplainEntity> getComplains()
    {
        return complains;
    }

    /**
     * @param complains
     *            the complains to set
     */
    public void setComplains(List<ComplainEntity> complains)
    {
        this.complains = complains;
    }
}
