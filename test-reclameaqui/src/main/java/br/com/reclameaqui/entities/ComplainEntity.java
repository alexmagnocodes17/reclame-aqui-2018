package br.com.reclameaqui.entities;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * @author Magno
 *
 */
@Entity
public class ComplainEntity extends BaseEntity
{    
    @Column(name = "complainant_id")
    private Long complainantId;

    @Column(name = "title", nullable = false, length = 64)
    private String title;

    @Column(name = "description", nullable = false, length = 64)
    private String description;

    @Column(name = "locale", nullable = false, length = 64)
    private String locale;

    @Column(name = "company", nullable = false, length = 64)
    private String company;

    public ComplainEntity()
    {
        // TODO Auto-generated constructor stub
    }

    public ComplainEntity(String title, String description, String locale, String company)
    {
        setTitle(title);
        setDescription(description);
        setLocale(locale);
        setCompany(company);
    }

    /**
     * @return the complainantId
     */
    public Long getComplainantId()
    {
        return complainantId;
    }

    /**
     * @param complainantId the complainantId to set
     */
    public void setComplainantId(Long complainantId)
    {
        this.complainantId = complainantId;
    }

    /**
     * @return the title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * @param title
     *            the title to set
     */
    public void setTitle(String title)
    {
        this.title = title;
    }

    /**
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * @return the locale
     */
    public String getLocale()
    {
        return locale;
    }

    /**
     * @param locale
     *            the locale to set
     */
    public void setLocale(String locale)
    {
        this.locale = locale;
    }

    /**
     * @return the company
     */
    public String getCompany()
    {
        return company;
    }

    /**
     * @param company
     *            the company to set
     */
    public void setCompany(String company)
    {
        this.company = company;
    }
}
