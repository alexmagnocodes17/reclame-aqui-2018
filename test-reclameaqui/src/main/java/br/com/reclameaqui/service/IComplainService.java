package br.com.reclameaqui.service;

import java.util.List;

import br.com.reclameaqui.dto.ComplainDTO;
import br.com.reclameaqui.entities.ComplainEntity;
import br.com.reclameaqui.exceptions.EntityNotFoundException;

public interface IComplainService extends ICommonService<ComplainDTO, ComplainEntity>
{
    void save(ComplainDTO complainDTO);

    List<ComplainDTO> listDTOS();

    ComplainDTO find(Long complainId) throws EntityNotFoundException;

    List<ComplainEntity> listEntities(String company, String locale);
}
