package br.com.reclameaqui.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.reclameaqui.dto.ComplainDTO;
import br.com.reclameaqui.entities.ComplainEntity;
import br.com.reclameaqui.exceptions.EntityNotFoundException;
import br.com.reclameaqui.mapper.ComplainMapper;
import br.com.reclameaqui.repository.RedisComplainRepository;

@Service
public class RedisComplainService
{
    @Autowired
    private RedisComplainRepository repository;

    public void save(ComplainDTO complainDTO)
    {
        ComplainEntity entity = ComplainMapper.makeComplainEntity(complainDTO);
        long nextLong = new Random().nextLong() % 1000;
        entity.setId(Math.abs(nextLong));
        entity.setCreationDate(new Timestamp(Calendar.getInstance().getTimeInMillis()));
        repository.save(entity);
    }

    public List<ComplainDTO> listDTOS()
    {
        Iterable<ComplainEntity> iterable = this.repository.findAll();
        List<ComplainDTO> list = new ArrayList<ComplainDTO>();
        for (Iterator<ComplainEntity> iterator = iterable.iterator(); iterator.hasNext();)
        {
            list.add(ComplainMapper.makeComplainDTO(iterator.next()));
        }
        return list;
    }

    public ComplainDTO find(Long id) throws EntityNotFoundException
    {
        ComplainEntity entity = this.repository.findById(id)
                        .orElseThrow(() -> new EntityNotFoundException("Could not find entity(Complain) with id: " + id));
        return ComplainMapper.makeComplainDTO(entity);
    }

/*    public List<ComplainDTO> listComplainsByCompanyAndLocale(String company, String locale)
    {
        Iterable<ComplainEntity> iterable = this.repository.findByCompanyAndLocale(company, locale);
        List<ComplainDTO> list = new ArrayList<ComplainDTO>();
        for (Iterator<ComplainEntity> iterator = iterable.iterator(); iterator.hasNext();)
        {
            list.add(ComplainMapper.makeComplainDTO(iterator.next()));
        }
        return list;
    }

    public List<ComplainEntity> listEntities(String company, String locale)
    {
        Iterable<ComplainEntity> iterable = this.repository.findByCompanyAndLocale(company, locale);
        List<ComplainEntity> list = new ArrayList<ComplainEntity>();
        for (Iterator<ComplainEntity> iterator = iterable.iterator(); iterator.hasNext();)
        {
            list.add(iterator.next());
        }
        return list;
    }*/
    
    public List<ComplainEntity> listEntities()
    {
        Iterable<ComplainEntity> iterable = this.repository.findAll();
           List<ComplainEntity> list = new ArrayList<ComplainEntity>();
        for (Iterator<ComplainEntity> iterator = iterable.iterator(); iterator.hasNext();)
        {
            list.add(iterator.next());
        }
        return list;
    }

    public List<ComplainEntity> findByCompanyAndLocale(String company, String locale) {
        return this.repository.findByCompanyAndLocale(company, locale);
    }
}
