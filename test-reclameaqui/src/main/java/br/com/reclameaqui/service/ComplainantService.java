package br.com.reclameaqui.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.reclameaqui.dto.ComplainantDTO;
import br.com.reclameaqui.entities.ComplainantEntity;
import br.com.reclameaqui.exceptions.EntityNotFoundException;
import br.com.reclameaqui.mapper.ComplainantMapper;
import br.com.reclameaqui.repository.ComplainantRepository;

@Service
public class ComplainantService implements IComplainantService
{  
    @Autowired
    private ComplainantRepository repository;

    @Override
    public void save(ComplainantDTO complainantDTO)
    {
        ComplainantEntity entity = ComplainantMapper.makeComplainantEntity(complainantDTO);
        repository.save(entity);
    }

    @Override
    public List<ComplainantDTO> listDTOS()
    {
        Iterable<ComplainantEntity> iterable = this.repository.findAll();
        List<ComplainantDTO> list = new ArrayList<ComplainantDTO>();
        for (Iterator<ComplainantEntity> iterator = iterable.iterator(); iterator.hasNext();)
        {
            list.add(ComplainantMapper.makeComplainantDTO(iterator.next()));
        }
        return list;
    }

    @Override
    public ComplainantDTO find(Long id) throws EntityNotFoundException
    {
        ComplainantEntity entity = this.repository.findById(id)
                        .orElseThrow(() -> new EntityNotFoundException("Could not find entity(Complain) with id: " + id));
        return ComplainantMapper.makeComplainantDTO(entity);
    }

    @Override
    public List<ComplainantEntity> listEntities()
    {
        Iterable<ComplainantEntity> iterable = this.repository.findAll();
        List<ComplainantEntity> list = new ArrayList<ComplainantEntity>();
        for (Iterator<ComplainantEntity> iterator = iterable.iterator(); iterator.hasNext();)
        {
            list.add(iterator.next());
        }
        return list;
    }
}
