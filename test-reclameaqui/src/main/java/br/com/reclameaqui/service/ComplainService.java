package br.com.reclameaqui.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.reclameaqui.dto.ComplainDTO;
import br.com.reclameaqui.entities.ComplainEntity;
import br.com.reclameaqui.exceptions.EntityNotFoundException;
import br.com.reclameaqui.mapper.ComplainMapper;
import br.com.reclameaqui.repository.ComplainRepository;

@Service
public class ComplainService implements IComplainService
{
    @Autowired
    private ComplainRepository complainRepository;

    @Override
    public void save(ComplainDTO complainDTO)
    {
        ComplainEntity entity = ComplainMapper.makeComplainEntity(complainDTO);
        complainRepository.save(entity);
    }

    @Override
    public List<ComplainDTO> listDTOS()
    {
        Iterable<ComplainEntity> iterable = this.complainRepository.findAll();
        List<ComplainDTO> list = new ArrayList<ComplainDTO>();
        for (Iterator<ComplainEntity> iterator = iterable.iterator(); iterator.hasNext();)
        {
            list.add(ComplainMapper.makeComplainDTO(iterator.next()));
        }
        return list;
    }

    @Override
    public ComplainDTO find(Long id) throws EntityNotFoundException
    {
        ComplainEntity entity = this.complainRepository. findById(id)
                        .orElseThrow(() -> new EntityNotFoundException("Could not find entity(Complain) with id: " + id));
        return ComplainMapper.makeComplainDTO(entity);
    }

    public List<ComplainDTO> listComplainsByCompanyAndLocale(String company, String locale)
    {
        Iterable<ComplainEntity> iterable = this.complainRepository.findByCompanyAndLocale(company, locale);
        List<ComplainDTO> list = new ArrayList<ComplainDTO>();
        for (Iterator<ComplainEntity> iterator = iterable.iterator(); iterator.hasNext();)
        {
            list.add(ComplainMapper.makeComplainDTO(iterator.next()));
        }
        return list;
    }

    public List<ComplainEntity> listEntities(String company, String locale)
    {
        Iterable<ComplainEntity> iterable = this.complainRepository.findByCompanyAndLocale(company, locale);
        List<ComplainEntity> list = new ArrayList<ComplainEntity>();
        for (Iterator<ComplainEntity> iterator = iterable.iterator(); iterator.hasNext();)
        {
            list.add(iterator.next());
        }
        return list;
    }
}
