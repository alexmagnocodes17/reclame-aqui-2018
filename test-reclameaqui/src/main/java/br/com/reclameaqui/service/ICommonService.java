package br.com.reclameaqui.service;

import java.util.List;

import br.com.reclameaqui.dto.BaseDTO;
import br.com.reclameaqui.entities.BaseEntity;
import br.com.reclameaqui.exceptions.EntityNotFoundException;

public interface ICommonService<K extends BaseDTO, V extends BaseEntity>
{
    void save(K k);
    
    List<K> listDTOS();
    
    K find(Long id) throws EntityNotFoundException;

}
