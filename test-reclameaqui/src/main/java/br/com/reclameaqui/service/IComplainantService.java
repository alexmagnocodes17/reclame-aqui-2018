package br.com.reclameaqui.service;

import java.util.List;

import br.com.reclameaqui.dto.ComplainantDTO;
import br.com.reclameaqui.entities.ComplainantEntity;
import br.com.reclameaqui.exceptions.EntityNotFoundException;

public interface IComplainantService extends ICommonService<ComplainantDTO, ComplainantEntity>
{
    void save(ComplainantDTO complainantDTO);

    List<ComplainantDTO> listDTOS();

    ComplainantDTO find(Long id) throws EntityNotFoundException;

    List<ComplainantEntity> listEntities();
}
