package br.com.reclameaqui.mapper;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import br.com.reclameaqui.dto.ComplainantDTO;
import br.com.reclameaqui.entities.ComplainantEntity;

public class ComplainantMapper
{
    public static ComplainantEntity makeComplainantEntity(ComplainantDTO complainantDTO)
    {
        return new ComplainantEntity(complainantDTO.getName(), complainantDTO.getEmail());
    }

    public static ComplainantDTO makeComplainantDTO(ComplainantEntity complainantEntity)
    {
        ComplainantDTO.ComplainantDTOBuilder complainDTOBuilder = ComplainantDTO.newBuilder()
                        .setName(complainantEntity.getName())
                        .setEmail(complainantEntity.getEmail());

        ComplainantDTO createComplainDTO = complainDTOBuilder.createComplainantDTO();
        return createComplainDTO;
    }

    public static List<ComplainantDTO> makeComplainDTOList(Collection<ComplainantEntity> complains)
    {
        return complains.stream().map(ComplainantMapper::makeComplainantDTO).collect(Collectors.toList());
    }
}
