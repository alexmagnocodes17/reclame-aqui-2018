package br.com.reclameaqui.mapper;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import br.com.reclameaqui.dto.ComplainDTO;
import br.com.reclameaqui.entities.ComplainEntity;

public class ComplainMapper
{
    public static ComplainEntity makeComplainEntity(ComplainDTO complainDTO)
    {
        return new ComplainEntity(complainDTO.getTitle(),
                        complainDTO.getDescription(),
                        complainDTO.getLocale(),
                        complainDTO.getCompany());
    }

    public static ComplainDTO makeComplainDTO(ComplainEntity complainEntity)
    {
        ComplainDTO.ComplainDTOBuilder complainDTOBuilder = ComplainDTO.newBuilder()
                        .setTitle(complainEntity.getTitle())
                        .setCompany(complainEntity.getCompany())
                        .setDescription(complainEntity.getDescription())
                        .setLocale(complainEntity.getLocale());

        ComplainDTO createComplainDTO = complainDTOBuilder.createComplainDTO();
        return createComplainDTO;
    }

    public static List<ComplainDTO> makeComplainDTOList(Collection<ComplainEntity> complains)
    {
        return complains.stream().map(ComplainMapper::makeComplainDTO).collect(Collectors.toList());
    }
}
