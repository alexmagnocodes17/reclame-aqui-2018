package br.com.reclameaqui.dto;

public class ComplainDTO extends BaseDTO
{
    private String title;
    private String description;
    private String locale;
    private String company;

    public ComplainDTO()
    {
        // TODO Auto-generated constructor stub
    }

    public ComplainDTO(String title, String description, String locale, String company)
    {
        setTitle(title);
        setDescription(description);
        setLocale(locale);
        setCompany(company);
    }

    public static ComplainDTOBuilder newBuilder()
    {
        return new ComplainDTOBuilder();
    }

    /**
     * @return the title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * @param title
     *            the title to set
     */
    public void setTitle(String title)
    {
        this.title = title;
    }

    /**
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * @return the locale
     */
    public String getLocale()
    {
        return locale;
    }

    /**
     * @param locale
     *            the locale to set
     */
    public void setLocale(String locale)
    {
        this.locale = locale;
    }

    /**
     * @return the company
     */
    public String getCompany()
    {
        return company;
    }

    /**
     * @param company
     *            the company to set
     */
    public void setCompany(String company)
    {
        this.company = company;
    }

    public static class ComplainDTOBuilder
    {
        private String title;
        private String description;
        private String locale;
        private String company;


        /**
         * @param title
         *            the title to set
         */
        public ComplainDTOBuilder setTitle(String title)
        {
            this.title = title;
            return this;
        }


        /**
         * @param description
         *            the description to set
         */
        public ComplainDTOBuilder setDescription(String description)
        {
            this.description = description;
            return this;
        }


        /**
         * @param locale
         *            the locale to set
         */
        public ComplainDTOBuilder setLocale(String locale)
        {
            this.locale = locale;
            return this;
        }


        /**
         * @param company
         *            the company to set
         */
        public ComplainDTOBuilder setCompany(String company)
        {
            this.company = company;
            return this;
        }


        public ComplainDTO createComplainDTO()
        {
            return new ComplainDTO(title, description, locale, company);
        }

    }
}
