package br.com.reclameaqui.dto;

import java.util.List;

public class ComplainantDTO extends BaseDTO
{
    private String name;

    private String email;

    private List<ComplainDTO> complains;

    public ComplainantDTO()
    {
        // TODO Auto-generated constructor stub
    }

    public ComplainantDTO(String name, String email)
    {
        setName(name);
        setEmail(email);
    }

    public static ComplainantDTOBuilder newBuilder()
    {
        return new ComplainantDTOBuilder();
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return the email
     */
    public String getEmail()
    {
        return email;
    }

    /**
     * @param email
     *            the email to set
     */
    public void setEmail(String email)
    {
        this.email = email;
    }

    /**
     * @return the complains
     */
    public List<ComplainDTO> getComplains()
    {
        return complains;
    }

    /**
     * @param complains
     *            the complains to set
     */
    public void setComplains(List<ComplainDTO> complains)
    {
        this.complains = complains;
    }

    public void addComplain(ComplainDTO complainDTO)
    {
        getComplains().add(complainDTO);
    }

    public static class ComplainantDTOBuilder
    {
        private String name;
        private String email;

        /**
         * @return the name
         */
        public String getName()
        {
            return name;
        }

        /**
         * @param name
         *            the name to set
         */
        public ComplainantDTOBuilder setName(String name)
        {
            this.name = name;
            return this;
        }

        /**
         * @return the email
         */
        public String getEmail()
        {
            return email;
        }

        /**
         * @param email
         *            the email to set
         */
        public ComplainantDTOBuilder setEmail(String email)
        {
            this.email = email;
            return this;
        }

        public ComplainantDTO createComplainantDTO()
        {
            return new ComplainantDTO(name, email);
        }
    }
}
