package br.com.reclameaqui.dto;

import java.util.List;

public class SummaryDTO
{
    private List<ComplainDTO> list;

    private int size;

    public SummaryDTO(List<ComplainDTO> list)
    {
        this.list = list;
        size = this.list!= null && this.list.size() > 0 ? this.list.size() : 0;
    }

    /**
     * @return the list
     */
    public List<ComplainDTO> getList()
    {
        return list;
    }

    /**
     * @return the size
     */
    public int getSize()
    {
        return size;
    }

}
