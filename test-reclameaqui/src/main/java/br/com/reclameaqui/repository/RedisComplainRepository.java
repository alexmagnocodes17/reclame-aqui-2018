package br.com.reclameaqui.repository;

import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import br.com.reclameaqui.entities.ComplainEntity;

@Repository
public class RedisComplainRepository
{
    private static final String KEY = "COMPLAIN";

    private RedisTemplate<String, Object> redisTemplate;
    private HashOperations<String, Long, ComplainEntity> hashOperations;

    @Autowired
    public RedisComplainRepository(RedisTemplate<String, Object> redisTemplate)
    {
        this.redisTemplate = redisTemplate;
    }

    @PostConstruct
    private void init()
    {
        hashOperations = redisTemplate.opsForHash();
    }

    public void save(ComplainEntity complain)
    {
        String key = String.join(":", KEY, complain.getLocale(), complain.getCompany());
        hashOperations.put(key, complain.getId(), complain);
    }

    public Optional<ComplainEntity> findById(Long id)
    {
        Optional<ComplainEntity> opt = Optional.of(hashOperations.get(KEY, id));
        return opt;
    }

    public List<ComplainEntity> findAll()
    {
        return hashOperations.values(KEY);
    }

    public List<ComplainEntity> findByCompanyAndLocale(String company, String locale) {
        String key = String.join(":", KEY, locale, company);
        System.out.println("Looking list by key[" + key + "].");
        return hashOperations.values(key);
    }

    public void update(ComplainEntity complain)
    {
        hashOperations.put(KEY, complain.getId(), complain);
    }

    public void delete(Long id)
    {
        hashOperations.delete(KEY, id);
    }
}
