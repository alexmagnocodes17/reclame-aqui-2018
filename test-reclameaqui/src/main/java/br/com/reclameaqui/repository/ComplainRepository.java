package br.com.reclameaqui.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.reclameaqui.entities.ComplainEntity;

@Repository
public interface ComplainRepository extends CrudRepository<ComplainEntity, Long>
{    
    List<ComplainEntity> findByCompanyAndLocale(String company, String locale);
}
