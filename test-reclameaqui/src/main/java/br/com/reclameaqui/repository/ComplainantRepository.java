package br.com.reclameaqui.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.reclameaqui.entities.ComplainantEntity;

@Repository
public interface ComplainantRepository extends CrudRepository<ComplainantEntity, Long>
{
    ComplainantEntity findByName(String name);
}
