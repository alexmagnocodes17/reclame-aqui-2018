package br.com.reclameaqui.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.reclameaqui.dto.ComplainantDTO;
import br.com.reclameaqui.entities.ComplainantEntity;
import br.com.reclameaqui.exceptions.EntityNotFoundException;
import br.com.reclameaqui.service.ComplainantService;

@RestController
@RequestMapping("v1/complainants")
public class ComplainantController
{
    private final ComplainantService service;

    @Autowired
    public ComplainantController(final ComplainantService service)
    {
        this.service = service;
    }

    @PostMapping("save")
    @ResponseStatus(HttpStatus.CREATED)
    public void saveComplainant(@RequestBody ComplainantDTO complainantDTO)
    {
        this.service.save(complainantDTO);
    }

    @GetMapping
    public List<ComplainantDTO> getcomplainants()
    {
        return this.service.listDTOS();
    }   
    
    @GetMapping("/{id}")
    public ComplainantDTO getComplainant(@Valid @PathVariable long id) throws EntityNotFoundException
    {
        return service.find(id);
    }    
  
    @GetMapping("findAll")
    public List<ComplainantEntity> listEntities()
                    throws EntityNotFoundException
    {
        return service.listEntities();
    }
}
