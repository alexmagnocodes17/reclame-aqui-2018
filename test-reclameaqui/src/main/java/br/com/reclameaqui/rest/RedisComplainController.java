package br.com.reclameaqui.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.reclameaqui.dto.ComplainDTO;
import br.com.reclameaqui.entities.ComplainEntity;
import br.com.reclameaqui.exceptions.EntityNotFoundException;
import br.com.reclameaqui.service.RedisComplainService;

@RestController
@RequestMapping("v1/redis/complains")
public class RedisComplainController
{
    private final RedisComplainService service;

    @Autowired
    public RedisComplainController(final RedisComplainService service)
    {
        this.service = service;
    }

    @PostMapping("save")
    @ResponseStatus(HttpStatus.CREATED)
    public void saveComplain(@RequestBody ComplainDTO complainDTO)
    {
        this.service.save(complainDTO);
    }

    @GetMapping("entities")
    public List<ComplainEntity> getEntities()
    {
        return this.service.listEntities();
    }

    @GetMapping
    public List<ComplainDTO> getcomplains()
    {
        return this.service.listDTOS();
    }

    @GetMapping("/{complainId}")
    public ComplainDTO getComplain(@Valid @PathVariable long id) throws EntityNotFoundException
    {
        return service.find(id);
    }

    /*
     * @GetMapping("searchBy") public SummaryDTO
     * listComplainsByCompanyAndLocale(@Valid @RequestParam("locale") String
     * locale,
     * 
     * @RequestParam("company") String company) throws EntityNotFoundException {
     * return new SummaryDTO(service.listComplainsByCompanyAndLocale(company,
     * locale)); }
     * 
     */ 
    @GetMapping("searchBy")
    public List<ComplainEntity> listEntities(@Valid @RequestParam("locale") String locale,
                    @RequestParam("company") String company) throws EntityNotFoundException
    {
        return service.findByCompanyAndLocale(company, locale);
    }

}
