package br.com.reclameaqui.rest;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.reclameaqui.dto.ComplainDTO;
import br.com.reclameaqui.dto.SummaryDTO;
import br.com.reclameaqui.entities.ComplainEntity;
import br.com.reclameaqui.exceptions.EntityNotFoundException;
import br.com.reclameaqui.service.ComplainService;

@RestController
@RequestMapping("v1/complains")
public class ComplainController
{
    private final ComplainService complainService;

    @Autowired
    public ComplainController(final ComplainService complainService)
    {
        this.complainService = complainService;
    }

    @PostMapping("save")
    @ResponseStatus(HttpStatus.CREATED)
    public void saveComplain(@RequestBody ComplainDTO complainDTO)
    {
        this.complainService.save(complainDTO);
    }
    
    @GetMapping
    public List<ComplainDTO> getComplains()
    {
        return this.complainService.listDTOS();
    }

    @GetMapping("/{id}")
    public ComplainDTO getComplain(@Valid @PathVariable long id) throws EntityNotFoundException
    {
        return complainService.find(id);
    }

    @GetMapping("searchBy")
    public SummaryDTO listComplainsByCompanyAndLocale(@Valid @RequestParam("locale") String locale,
                    @RequestParam("company") String company)
                    throws EntityNotFoundException
    {
        return new SummaryDTO(complainService.listComplainsByCompanyAndLocale(company, locale));
    }

    @GetMapping("findAll")
    public List<ComplainEntity> listEntities(@Valid @RequestParam("locale") String locale,
                    @RequestParam("company") String company)
                    throws EntityNotFoundException
    {
        return complainService.listEntities(company, locale);
    }
}
