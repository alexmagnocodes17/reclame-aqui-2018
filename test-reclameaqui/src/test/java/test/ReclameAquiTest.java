package test;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.reclameaqui.Application;
import br.com.reclameaqui.entities.ComplainantEntity;
import br.com.reclameaqui.repository.ComplainantRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@DataJpaTest
public class ReclameAquiTest
{
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ComplainantRepository repository;

    // write test cases here
    @Test
    public void findComplainant()
    {
        // given
        ComplainantEntity jp = new ComplainantEntity("JOAO JOSE", "jjose@gmail.com");
        entityManager.persist(jp);
        entityManager.flush();

        // when
        ComplainantEntity found = repository.findByName(jp.getName());

        // then
        assertThat(found.getName(), is("MAGNO"));
    }

}